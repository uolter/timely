# script which starts the redis job worker after the deploy is completed.

cd ${OPENSHIFT_REPO_DIR}

# worker
rqworker -H ${OPENSHIFT_REDIS_HOST} -p ${OPENSHIFT_REDIS_PORT} -a ${REDIS_PASSWORD} > /dev/null 2>&1 &
