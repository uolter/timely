# script which starts the redis job scheduler after the deploy is completed.

cd ${OPENSHIFT_REPO_DIR}

# scheduler
rqscheduler -H ${OPENSHIFT_REDIS_HOST} -p ${OPENSHIFT_REDIS_PORT} -P ${REDIS_PASSWORD} > /dev/null 2>&1 &
