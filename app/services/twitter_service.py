# -*- coding: utf8 -*-

import twitter

class TwitterService(object):


    def __init__(self, config):
        try:
            self.api = twitter.Api(
                consumer_key=config.TWITTER_CONSUMER_KEY,
                consumer_secret=config.TWITTER_CONSUMER_SECRET,
                access_token_key=config.TWITTER_ACCESS_TOKEN,
                access_token_secret=config.TWITTER_ACCESS_TOKEN_SECRET
            )
        except Exception, e:
            raise TwitterServiceException(e)


    def post_message(self, message):
        """ post a twitter message """
        try:
            self.api.PostUpdate(message)
        except Exception, e:
            raise TwitterServiceException(e)


class TwitterServiceException(Exception):
    
    def __init__(self, message):
        self.message = message

    def __str__(self):        
        return "[Twitter Service ERROR] %s\n" % str(self.message)