# -*- coding: utf8 -*-
from app import app
import requests
import urlparse
import json


class BitlyService(object):
    __api__ = 'v3/user/link_history'

    @property
    def endpoint(self):
        return urlparse.urljoin(app.config['BITLY_URL'], self.__api__)
    
    
    def timeline(self, query=None):
        """ Return the list of the latest shortened link_history
        """

        query_params = {
            'access_token': app.config['BITLY_TOKEN'],
            'limit': app.config['BITLY_MAX_RESULT']}
        if query:
            query_params['query'] = query

        response = requests.get(
            self.endpoint, 
            params=query_params, 
            verify=False)

        data = json.loads(response.content)
        history =  data['data']['link_history']
    
        return [(h['title'], h['link']) for h in history]




