# -*- coding: utf8 -*-

from rq import use_connection
from rq_scheduler import Scheduler
from datetime import datetime, timedelta
import time
import calendar
from redis import Redis



class JobScheduler(object):

    def __init__(self, config):

        use_connection() # Use RQ's default Redis connection
        self.config = config
        if self.config['REDIS_HOST'] and self.config['REDIS_HOST']:
            self.scheduler = Scheduler('default',
                connection=Redis(
                    self.config['REDIS_HOST'],
                    port=self.config['REDIS_PORT'],
                    password=self.config['REDIS_PASSWORD']))
        else:
            self.scheduler = Scheduler() # Get a scheduler for the "default" queue


    def post(self, start_at, func, message):
        """ schedule the function func at the time date """

        current_time = datetime.now()

        # convert to unix timestamp
        d1_ts = time.mktime(current_time.timetuple())
        d2_ts = time.mktime(start_at.timetuple())

        minutes = int((d2_ts-d1_ts) / 60)

        try:
            job = self.scheduler.enqueue_in(
                timedelta(minutes=minutes),
                func,
                message)

            return job.id, minutes

        except Exception, e:
            raise JobSchedulerException(e)

    def job_list(self, with_times=False):
        """ return the job list in the scheduler """

        try:
            if with_times:
                return [(j[0], utc_to_local(j[1])) for j in self.scheduler.get_jobs(with_times=with_times)]

            return self.scheduler.get_jobs(with_times=with_times)

        except Exception, e:
            raise JobSchedulerException(e)

    def cancel(self, job_id):
        """ delete from the queue the job identified with job_id """

        try:
            self.scheduler.cancel(job_id)
        except Exception, e:
            raise JobSchedulerException(e)

    def run(self, job_id):
        """ processes the job id """

        try:
            jobs = self.job_list()
            for j in jobs:
                if j.id == job_id:
                    print 'Run ', j.id
                    self.scheduler.enqueue_job(j)

            # (self.scheduler.enqueue_job(j) for j in jobs if j.id == job_id)

        except Exception, e:
            raise JobSchedulerException(e)



class JobSchedulerException(Exception):

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return "[Job Scheduler ERROR] %s\n" % str(self.message)


def utc_to_local(utc_dt):
    # get integer timestamp to avoid precision lost
    timestamp = calendar.timegm(utc_dt.timetuple())
    local_dt = datetime.fromtimestamp(timestamp)
    assert utc_dt.resolution >= timedelta(microseconds=1)
    return local_dt.replace(microsecond=utc_dt.microsecond)
