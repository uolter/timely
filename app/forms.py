# -*- coding: utf-8 -*-

from flask.ext.wtf import Form
from wtforms import TextAreaField, DateTimeField, validators
from datetime import datetime


class ShareForm(Form):

    share_txt = TextAreaField(validators=[validators.DataRequired(),  validators.length(max=140)])
    share_at = DateTimeField(default=datetime.now(), format='%d-%m-%Y %H:%M:%S')