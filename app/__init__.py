from flask import Flask
import os

app = Flask(__name__)
app.config.from_object('app.config')

# set the time zone:
os.environ['TZ'] = app.config['TIMEZONE']

if not app.debug:
    from logging.handlers import RotatingFileHandler

    handler = RotatingFileHandler(
            app.config['LOG_FILENAME'],
            maxBytes=app.config['LOG_MAX_BYTE'],
            backupCount= app.config['LOG_BACKUP_COUNT']
    )

    app.logger.addHandler(handler)
    app.logger.setLevel(app.config['LOG_LEVEL'])


from app import views
