# -*- coding: utf8 -*-

from app import app
from flask import render_template, redirect, url_for, request
from services.bitly_service import BitlyService
from services.sheduler_service import JobScheduler, JobSchedulerException
from forms import ShareForm
from job import schedule
from security import requires_auth


@app.route('/index', methods=['GET', 'POST'])
@requires_auth
def index():

    # get the bitly timeline

    app.logger.debug('call index()')

    service = BitlyService()
    form = ShareForm()

    if request.method == 'POST' and form.validate_on_submit():
        app.logger.info('posting')
        try:
            job_scheduler = JobScheduler(app.config)
            job_id, minutes = job_scheduler.post(form.share_at.data, schedule, form.share_txt.data)
            app.logger.info('scheduled job %s at %s (in %d minutes)' % (job_id, form.share_at.data, minutes))
        except JobSchedulerException, e:
            app.logger.error(e)
            # TODO: show the error page.

    query = request.args.get('search', '')
    return render_template('index.html', timeline=service.timeline(query), form=form)


@app.route('/jobs')
@requires_auth
def jobs_list():
    """ return the jobs page """

    try:
        job_scheduler = JobScheduler(app.config)
        jobs = job_scheduler.job_list(with_times=True)

    except JobSchedulerException, e:
            app.logger.error(e)
            # TODO: show the error page.

    return render_template('jobs.html', jobs=jobs )


@app.route('/job/delete/<job_id>')
@requires_auth
def delete_job(job_id):
    """ delete the seleced job and redirects to the list of jobs """

    try:
        app.logger.debug('Deleting job %s' % job_id)
        job_scheduler = JobScheduler(app.config)
        job_scheduler.cancel(job_id)

    except JobSchedulerException, e:
            app.logger.error(e)
            # TODO: show the error page.

    return redirect(url_for('jobs_list'))


@app.route('/job/start/<job_id>')
@requires_auth
def start_job(job_id):
    """ force the execution fo the selected job """

    try:
        job_scheduler = JobScheduler(app.config)
        job_scheduler.run(job_id)

    except JobSchedulerException, e:
            app.logger.error(e)
            # TODO: show the error page.

    return redirect(url_for('jobs_list'))
