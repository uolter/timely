# -*- coding: utf-8 -*-

# Time zone
TIMEZONE = 'Europe/Rome'

CSRF_ENABLED = True
SECRET_KEY = '<your private key here>'

BITLY_URL = 'https://api-ssl.bitly.com'
BITLY_TOKEN = 'get your bitly token'  # http://dev.bitly.com/
# number of bitly links shown in the page
BITLY_MAX_RESULT = 10

########### Twitter API Configuration ###########

# get your twitter keys and token @ https://dev.twitter.com/
TWITTER_CONSUMER_KEY=''
TWITTER_CONSUMER_SECRET=''
TWITTER_ACCESS_TOKEN=''
TWITTER_ACCESS_TOKEN_SECRET=''

########### Basic Authentication ###########

BASIC_USERNAME='admin'
BASIC_PASSWORD='admin'


########### Redis Connection ###########
# in order to authenticate use: <password>@<host>
REDIS_HOST = 'localhost'
REDIS_PORT = 6379

########## Logging ###############
import logging
LOG_LEVEL = logging.DEBUG
LOG_FILENAME = 'timely.log'
LOG_BACKUP_COUNT = 10
LOG_MAX_BYTE = 5 * 1024

try:
    # create the file config_env to overwrite the default configurations.
    from config_env import *
except ImportError:
    pass
