# -*- coding: utf-8 -*-
from services.twitter_service import TwitterService
import config

def schedule(message):

    print 'Start scheduler'
    try:
        twitter_service = TwitterService(config)
        twitter_service.post_message(message)
    except Exception, e:
        print e
    print 'Stop Scheduler'



if __name__ == '__main__':

    schedule('Test')