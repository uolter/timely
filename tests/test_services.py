import unittest
from app import config
from app.services.bitly_service import BitlyService
from app.services.twitter_service import TwitterService, TwitterServiceException
from mock import patch, Mock
from twitter import TwitterError

class mock_bitly_response():

    @property
    def content(self): 
        return '{"data":{"link_history": [{"title":"Mock Title", "link":"http://example.com"}]}}'



class TestBitlyService(unittest.TestCase):


    def setUp(self):
        self.service = BitlyService()


    def test_end_point(self):

        self.assertEqual(
            self.service.endpoint, 
            'https://api-ssl.bitly.com/v3/user/link_history'
            )

    @patch('app.services.bitly_service.requests')
    def test_timeline(self, requests):

        requests.get.return_value = mock_bitly_response()

        timeline = self.service.timeline()

        self.assertTrue(
            len(timeline) > 0
            )

        self.assertFalse(
            timeline[0][0]==None
            )

    def test_search_timeline(self):

        timeline = self.service.timeline('git')

        print timeline

        self.assertTrue(
            len(timeline) > 0
            )

        self.assertFalse(
            timeline[0][0]==None
            )


class TestTwitterService(unittest.TestCase):

    @patch('twitter.Api')
    def setUp(self, Api):
        Api.PostUpdate.return_value = None
        self.service = TwitterService(config)


    def test_post_message(self):

        self.service.post_message('Timely app test. Forget this message')

        self.assertTrue(True)

    def test_post_message_exception(self):

        self.service.api.PostUpdate = Mock(side_effect=TwitterError('foo'))
        try:
            self.service.post_message('Timely app test. Forget this message')
            self.assertTrue(False)
        except TwitterServiceException:
            self.assertTrue(True)


if __name__ == '__main__':

    unittest.main()